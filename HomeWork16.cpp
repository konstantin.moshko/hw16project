﻿#include <iostream>
#include <ctime>								//Библиотека времени

int main()
{
	const int n = 5;							//размерность массива
	
	time_t now = time(0);						//берём системное время
	tm* date = localtime(&now);
	int day = date->tm_mday;					//забираем сегодняшнюю дату
	
	int array[n][n];							
	int sum(0);									//переменная для сохранения суммы значений строки
	int testDay(day % n);						//переменная для сохранения остатка от деления даты на n
							

	for (int i = 0; i < n; i++)					//вывод массива
	{
		for (int j = 0; j < n; j++)
		{
					
			array[i][j] = i + j;
			std::cout << array[i][j]<<" ";

			if (i == testDay)					//если индекс совпадает с остатком деления, то сохраняем данные в sum
			{
				sum = sum + array[i][j];
				
			}
					}
		std::cout << "\n";
				
	}
	
	std::cout << "\n Summ: " << sum << "\n" << testDay;	//выводим сумму чисел и номер строки
	

	return 0;
}